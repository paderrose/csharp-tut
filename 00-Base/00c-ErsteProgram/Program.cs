﻿using System;

namespace _00c_ErsteProgram
{
    //Basis Klasse des C# Programms
    class Program
    {
        static void Main(string[] args)
        {
            /* Die Klasse Console representriert die Eingab und Ausgabe
            dieser Konsolen Anwendung dar.
            Mit der Methode "WriteLine" kann die Anwendung Daten für den Benutzer
            lesbar anzeigen.
            */
            Console.WriteLine("Hello World!");
        }
    }
}
