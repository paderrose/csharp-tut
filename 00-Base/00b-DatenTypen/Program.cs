﻿using System;

namespace _00c_DatenTypen
{
    class Program
    {
        static void Main(string[] args)
        {
           /* Damit eine Anwendung Daten verarbeiten kann, muss Speicher für
           diese Daten reserviert werden. Wie viel Speicher dafür benötigt wird,
            ist jedoch abhängig von den Typen der Daten, da nicht jeder Typ
            gleich viel Speicher belegt. Zudem wird in C#
           noch zwischen zwei Arten von Typen unterschieden, nämlich Werte-
           und Verweistypen.

          Wertetypen sind - wie ihr Name schon sagt - Typen, die Werte direkt
          speichern. Das heißt, wird von der Anwendung auf einen Wertetyp
          zugegriffen, dann werden die Daten direkt aus der entsprechenden
          Stelle im Speicher gelesen.
          Im Gegensatz dazu speichern Verweistypen nur die Adresse der
          Speicherstelle, an der die eigentlichen Daten abgelegt sind.
          Greift die Anwendung also auf einen Verweistyp zu, wird zunächst
          aus der entsprechenden Stelle im Speicher gelesen, wo sich die
          eigentlichen Daten befinden, woraufhin diese in einem zweiten Schritt
          dann von dort gelesen werden können.
          Diese zunächst aufwändig erscheinende Trennung in Werte- und
          Verweistypen liegt in der Größe der Daten begründet, die gespeichert
          werden sollen. Daten, deren Umfang im Voraus bekannt ist,
          werden in der Regel als Wertetyp abgelegt. Da Wertetypen - vereinfacht
           gesagt - in einer Tabelle im Speicher verwaltet werden, findet der
            Zugriff auf diese sehr schnell statt.*/

          /*Damit bei der Entwicklung von Anwendungen nicht jeder Typ vom
          Benutzer entwickelt werden muss, enthält C# eine Reihe vordefinierter
          Typen für einfache Daten, die automatisch in jeder Anwendung zur
          Verfügung stehen.
          Für Ganzzahlen bietet C# acht verschiedene Typen, die sich in erster
          Linie durch ihren Wertebereich unterscheiden. Der Wertebereich
          berechnet sich dabei aus der Anzahl der verfügbaren Bits, wobei
          nochmals zwischen vorzeichenbehafteten und vorzeichenfreien Typen
          unterschieden wird. Als Standardwert verwenden diese Typen die Zahl
          Null.*/


          //Type NameDerVariable	  Minimum	             Maximum
          sbyte sb;	              // -128	                127
          short	a;	              // -32.768	           32.767
          int	b;	                //-2.147.483.648	     2.147.483.647
          long	c;	              //-9.223.372.036.854.775.808	     9.223.372.036.854.775.807
          byte	v;	              //  0	                  255
          ushort	e;	            //0	                    65.535
          uint	f;	              //0	                    4.294.967.295
          ulong	g;	               //0                    18.446.744.073.709.551.615

          /*Für Dezimalzahlen bietet C# drei verschiedene Typen, die sich nicht
          nur durch ihren Wertebereich, sondern auch durch die Anzahl der
          verfügbaren Nachkommastellen unterscheiden. Die Typen float und double
          entsprechen dabei dem IEEE 754-Standard, der seit 1985 einen weltweit
          einheitlichen Standard zur Verarbeitung von Dezimalzahlen definiert.

          Der Typ decimal hingegen verfügt zwar über einen kleineren Wertebereich
          als float und double, dafür aber über eine deutlich höhere Genauigkeit.

         Als Besonderheit bieten die Typen float und double die Möglichkeit,
         die Werte +0 und -0, +∞, -∞ und NaN zu speichern. Die Werte +0 und -0
         sind vor allem beim Runden interessant. Neben +∞ und -∞ zur Darstellung
         positiver und negativer Unendlichkeit können float und double auch den
         Wert NaN - Not a Number - speichern, um ein mathematisch nicht definiertes
         Ergebnis abzubilden. Für decimal stehen diese besonderen Werte nicht
         zur Verfügung.*/

        //Type NameDerVariable	  Minimum	             Maximum
        float	  fi;               //±1,5 × 10-45	     ± 3,4 × 1038	32 Bit
        double	di;               //±5.0 × 10-324	     ±1.7 × 10308	64 Bit
        decimal	ddi;              //±1.0 × 10-28	     ±7.9 × 1028	128 Bit

        /*Schließlich unterstützt C# noch den Typ bool, der zur Darstellung der
        logischen Werte true und false dient. Die Werte true und false werden
        als Literale bezeichnet. Der Standardwert für diesen Typ ist false.*/
        bool bg; // true or false

        //Außer diesen Typen für Ganz- und Dezimalzahlen bietet C#
        //noch den Typ char zur Aufnahme eines einzelnen Zeichens.
        char ccst = 'b'; // Ein einzelnes Zeichen wird in C# dabei durch einfache Anführungszeichen eingeschlossen

        /*Alle bislang vorgestellten Typen sind Wertetypen, deren Speicherbedarf
        im Voraus bekannt ist. Außer diesen Typen enthält C# noch zwei
        Verweistypen, nämlich string und object.

        Der Typ string dient zur Aufnahme von Text, der aus beliebig vielen
        Zeichen bestehen kann.
        Ein Text wird in C# durch doppelte Anführungszeichen eingeschlossen.*/
        string Welt = "Hallo Welt!";

        /*Der Typ object schließlich spielt eine Sonderrolle, da alle anderen Typen
        von ihm abstammen. Daher kann er für jeden anderen Typ eingesetzt werden,
        das heißt, object kann einen Verweis auf beliebige Daten speichern.
        Zudem kann jeder Typ in object umgewandelt und von object wieder in den ursprünglichen Typ
        zurückgewandelt werden, was als Boxing beziehungsweise Unboxing
        bezeichnet wird.*/
        object obj = Welt ;

        
        }
    }
}
