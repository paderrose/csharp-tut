﻿using System; // DatenTypen , und alle Basis Objekte befinden sich im namespace System


/*C#, das als "ßiescharp [ˌsiːˈʃɑɹp]" ausgesprochen wird, ist eine Programmiersprache für .NET,
die von Microsoft in Zusammenarbeit mit dem Erfinder von Delphi,
 Anders Heijlsberg,speziell für diese Plattform entwickelt wurde und
 daher auch als Lingua Franca für .NET bezeichnet wird.

Der Name von C# lehnt sich in seiner Schreibweise an den in der Musik um einen
Halbton erhöhten Notenwert C namens Cis an, der ebenfalls als C# geschrieben wird,
und bezeichnet daher eine höhere Variante der Programmiersprache C.
 Außerdem kann C# so wohl als symbolische Anspielung an die Sprache C++ wie
 auch als Wortspiel "see sharp" gesehen werden.*/

/*Die Typen, die während der Entwicklung einer Anwendung entstehen, werden
jeweils mit einem beschreibenden Namen versehen, welche Art von Daten dieser Typ
verarbeiten soll. Allerdings kann es durch den Einsatz von Komponenten vorkommen,
dass zwei verschiedene Typen unabhängig voneinander den gleichen Namen tragen,
 wobei der Name innerhalb der jeweiligen Komponente eindeutig ist.

Um diese Typen dennoch unterscheiden zu können, werden sie üblicherweise in
sogenannten namespace's organisiert. Ein namespace stellt dabei einen Container
dar, der die in ihm enthaltenen Typen von den Typen anderer namespaces abschottet.
Wird ein Typ in keinen namespace eingeordnet, befindet er sich automatisch im
globalen namespace, der mit global::
bezeichnet wird.

Innerhalb eines Namensraumes reicht der Name eines Typs zu seiner Identifikation
aus. Typen, die sich in anderen Namensräumen befinden, müssen jedoch
zusätzlich mit ihrem zugehörigen namespace angesprochen werden.

Die Verschachtelung von Namensräumen wird in C# mit Hilfe des Punkt-Operators
durchgeführt*/

namespace _00a_Empty
{
    /*Da C# eine objektorientierte Sprache ist, wird am häufigsten das Konzept
    der Klasse zur Erstellung eigener Typen verwendet.
    Klassen sind Baupläne für Objekte.
    */
    class Program
    {
        //Die Main Methode ist der sogenannten EntryPoint also übersetzt die
        //Methode die als erstes Aufgerufen wird wenn ein C# Programm ausgeführt
        //wird
        static void Main(string[] args) {

        }
    }
}
