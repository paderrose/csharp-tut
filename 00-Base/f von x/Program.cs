﻿using System;

namespace _00_Base
{
    //Basis Klasse des C# Ürogramms
    class Program {
        //Entry Point - Erste Funktion die AUsgeführt wird
        static void Main(string[] args)
        {
            // Dekleration der Variable x und Initialisierung mit 0
            int x = 0;
            // Aufruf der Funktion Write der Klasse Console
            // Ausgabe auf der Console "Bitte gib die Zahl für x ein"
            Console.Write("Bitte gib die Zahl für x ein: ");
            // Lesen der Eingabe auf der Konsole und speichern in der Variable xstr; Wird als string ausgegeben
            // Dekleration Variable xstr als string
            string xstr = Console.ReadLine();
            // Wandeln (Parsen) des Strings in der Variable xstr in den Datentyp "Int" (Ganzzahl)
            x = int.Parse(xstr);
            // Ausgabe von f(x)=x+1={0} | {0} wird mit der Ergebnis von x + 1 ersetzt 
            Console.WriteLine("f(x)=x+1={0}", x + 1);
        }   
    }
}
